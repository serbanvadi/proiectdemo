const express=require('express');
const bodyParser=require('body-parser');
const Sequelize=require("sequelize");
var cors = require("cors");


const app=express();
app.use(cors());
app.use(bodyParser.json());

const sequelize=new Sequelize('c9','serbanadi','',{
    host:'localhost',
    dialect:'mysql',
    operatorsAliases:false,
    pool:{
        "max":1,
        "min":0,
        "idle":20000,
        "acquire":20000
    }
});

sequelize.authenticate().then(()=>{
    console.log('Conexiunea s-a realizat cu succces');
}).catch(err=>{
    console.error('Imposibil de conectat la baza de date',err);
});

const Utilizator = sequelize.define('utilizator',{
    nume:{
        type:Sequelize.STRING,
        allowNull:false
    },
    prenume:{
        type:Sequelize.STRING,
        allowNull:false
    },
    tara:{
      type:Sequelize.STRING,
      allowNull:false
    },
    utilizator:{
        type:Sequelize.STRING,
        allowNull:false,
        primaryKey:true
    },
    parola:{
        type:Sequelize.STRING,
        allowNull:false
    }
});

const Istoric =sequelize.define('istoric',{
    idIstoric:{
        type:Sequelize.INTEGER,
        allowNull:false,
        primaryKey:true
    },
    textCautat:{
        type:Sequelize.STRING,
        allowNull:false
    }
    //,
    // linkAccesat:{
    //     type:Sequelize.STRING,
    //     allowNull:false
    // }
   
});
Istoric.belongsTo(Utilizator);
const Bookmarks = sequelize.define('bookmarks',{
    idBookmark:{
        type: Sequelize.INTEGER,
        allowNull:false,
        primaryKey:true
    },
    linkFavorit:{
        type:Sequelize.STRING,
        allowNull:false
    }
});
Bookmarks.belongsTo(Utilizator);
const citateFavorite = sequelize.define('citatefavorite',{
    id_citat:{
        type:Sequelize.INTEGER,
        primaryKey:true,
        allowNull:false
    },
    citat:{
        type:Sequelize.STRING,
        allowNull:false
    }
});
citateFavorite.belongsTo(Utilizator);
sequelize.sync({force:true}).then(()=>{
    console.log('Baza de date s-a creat cu succes')
})

app.post('/inregistrare',(req,res)=>{
    Utilizator.create({
        nume:req.body.nume,
        prenume:req.body.prenume,
        tara:req.body.tara,
        utilizator:req.body.utilizator,
        parola:req.body.parola
    }).then((user)=>{
        res.status(200).send(user);
    }, (err)=>{
        res.status(500).send(err);
    })
});

app.put('/inregistrare/:utilizator',(req,res)=>{
    Utilizator.findById(req.params.utilizator).then((user)=>{
        if(user){
            user.update(req.body).then((result)=>{
                res.status(201).json(result)
            }).catch((err)=>{
                console.log(err);
                res.status(500).send('Eroare in baza de date');
            })
        }
        else{
            res.status(404).send('Resursa nu a fost gasita');
        }
    }).catch((err)=>{
        console.log(err);
        res.status(500).send('Eroare in baza de date');
    });
});

app.get('/get-all-users',(req,res)=>{
    Utilizator.findAll().then((users)=>{
        res.status(200).send(users);
    });
});

app.delete('/inregistrare/:utilizator',(req,res)=>{
    Utilizator.findById(req.params.utilizator).then((user)=>{
        if(user){
            user.destroy().then((result)=>{
                res.status(204).send()
            }).catch((err)=>{
                console.log(err);
                res.status(500).send('Eroare in baza de date');
            })
        }
        else{
            res.status(404).send('Resursa nu a fost gasita');
        }
    }).catch((err)=>{
        console.log(err);
        res.status(500).send('Eroare in baza de date');
    });
});

app.post('/istoric',(req,res)=>{
    Istoric.create({
        idIstoric:req.body.idIstoric,
        textCautat:req.body.textCautat//,
     //   linkAccesat:req.body.linkAccesat
       
    }).then((istoric)=>{
        res.status(200).send(istoric);
    },(err)=>{
        res.status(200).send(err);
    })
});

app.put('/istoric/:idIstoric',(req,res)=>{
    Istoric.findById(req.params.idIstoric).then((istoric)=>{
        if(istoric){
            istoric.update(req.body).then((result)=>{
                res.status(201).json(result)
            }).catch((err)=>{
                console.log(err);
                res.status(500).send('Eroare in baza de date');
            })
        }
        else{
            res.status(404).send('Resursa nu a fost gasita');
        }
    }).catch((err)=>{
        console.log(err)
        res.status(500).send('Eroare in baza de date');
    });
});

app.get('/get-all-history',(req,res)=>{
    Istoric.findAll().then((istoric)=>{
        res.status(200).send(istoric);
    });
});

app.delete('/istoric/:idIstoric',(req,res)=>{
    Istoric.findById(req.params.idIstoric).then((history)=>{
        if(history){
            history.destroy().then((result)=>{
                res.status(204).send();
            }).catch((err)=>{
                console.log(err);
                res.status(500).send('Eroare in baza de date');
            })
        }
        else{
            res.status(404).send('Resursa nu a fost gasita');
        }
    }).catch((err)=>{
        console.log(err);
        res.status(500).send('Eroare in baza de date');
    });
});

app.post('/bookmarks',(req,res)=>{
    Bookmarks.create({
        idBookmark:req.body.idBookmark,
        linkFavorit:req.body.linkFavorit
    }).then((bookmark)=>{
        res.status(200).send(bookmark);
    },(err)=>{
        res.status(200).send(err);
    })
});

app.put('/bookmarks/:idBookmark',(req,res)=>{
    Bookmarks.findById(req.params.idBookmark).then((bookmark)=>{
        if(bookmark){
            bookmark.update(req.body).then((result)=>{
                res.status(201).json(result)
            }).catch((err)=>{
                console.log(err);
                res.status(500).send('Eroare in baza de date');
            })
        }
        else{
            res.status(404).send('Resursa nu a fost gasita');
        }
    }).catch((err)=>{
        console.log(err);
        res.status(500).send('Eroare in baza de date');
    });
});

app.get('/get-all-bookmarks',(req,res)=>{
    Bookmarks.findAll().then((bookmarks)=>{
        res.status(200).send(bookmarks);
    });
});

app.delete('/bookmarks/:idBookmark',(req,res)=>{
    Bookmarks.findById(req.params.idBookmark).then((bookmark)=>{
        if(bookmark){
            bookmark.destroy().then((result)=>{
                res.status(204).send();
            }).catch((err)=>{
                console.log(err);
                res.status(500).send('Eroare in baza de date');
            })
        }
        else{
            res.status(404).send('Resursa nu a fost gasita');
        }
    }).catch((err)=>{
        console.log(err);
        res.status(500).send('Eroare in baza de date');
    });
});

app.post('/citateFavorite',(req,res)=>{
    citateFavorite.create({
        id_citat:req.body.id_citat,
        citat:req.body.citat
    }).then((citatefav)=>{
        res.status(200).send(citatefav);
    },(err)=>{
        res.status(200).send(err);
    });
});

app.put('/citateFavorite/:id_citat',(req,res)=>{
    citateFavorite.findById(req.params.id_citat).then((citat)=>{
        if(citat){
            citat.update(req.body).then((result)=>{
                res.status(201).json(result)
            }).catch((err)=>{
                console.log(err);
                res.status(500).send('Eroare in baza de date');
            })
        }
        else{
            res.status(404).send('Resursa nu a fost gasita');
        }
    }).catch((err)=>{
        console.log(err);
        res.status(500).send('Eroare in baza de date');
    });
});

app.get('/get-all-favourite-quotes',(req,res)=>{
    citateFavorite.findAll().then((fav)=>{
        res.status(200).send(fav);
    });
});

app.delete('/citateFavorite/:id_citat',(req,res)=>{
    citateFavorite.findById(req.params.id_citat).then((citat)=>{
        if(citat){
            citat.destroy().then((result)=>{
                res.status(204).send();
            }).catch((err)=>{
                console.log(err);
                res.status(500).send('Eroare in baza de date');
            })
        }
        else{
            res.status(404).send('Resursa nu a fost gasita');
        }
    }).catch((err)=>{
        console.log(err);
        res.status(500).send('Eroare in baza de date');
    });
});


app.listen(8080, () =>{
    console.log('Serverul a pornit pe portul 8080');
});

